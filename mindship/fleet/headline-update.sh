#!/bin/bash
echo "Headline script"

api (){
    url="https://hbm-staging.mindship.tech/query"
    #url="http://13.232.179.161:3000/query"
    #url="https://hbm-api.harbormoor.com/query"
    curl -sS --request POST \
        --url $url \
        --header 'Content-Type: application/json' \
        --data '{"query":"mutation{\n  updateHeadLine(input:{\n    pagination:{\n      page:'$1',\n      limit:'$2'\n    }\n  }){\n    totalPage\n    currentPage\n    totalRecords\n  }\n}"}'
}

page=1
limit=10
totalPage=$(api $page $limit | awk '{ split($1,a,","); print a[1]}' | awk '{split($1,a,":"); print a[4]}')
echo $totalPage
while [ $page -le $totalPage ]
do
    echo -n "Processing page $page of $totalPage ..."
    status=$(api $page $limit)
    echo "Status: Done"
    page=`expr $page + 1`
done
